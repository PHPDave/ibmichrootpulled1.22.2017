https://bitbucket.org/litmis/ibmichroot/src (see how_to_OPS_NODE.txt)
===============================
installation
===============================
CRTUSRPRF USRPRF(BOBBY) PASSWORD() USRCLS(*PGMR) TEXT('Tony Cairns')
CHGUSRPRF USRPRF(BOBBY) LOCALE(*NONE) HOMEDIR('/QOpenSys/bobby/./home/bobby')

-- outside chroot --
$ mkdir -p /QOpenSys/bobby/home/bobby
$ mkdir -p /QOpenSys/QIBM/ProdData/OPS/GCC
$ cd /QOpenSys/QIBM/ProdData/OPS/GCC
-- ftp from Yips until PTF available --
$ ./chroot_setup.sh chroot_minimal.lst /QOpenSys/bobby
$ ./chroot_setup.sh chroot_OPS_GCC.lst /QOpenSys/bobby
$ ./chroot_setup.sh chroot_OPS_SC1.lst /QOpenSys/bobby
$ ./chroot_setup.sh chroot_OPS_NODE.lst /QOpenSys/bobby
-- additional setup if another profile (careful must use chroot) --
$ chroot /QOpenSys/bobby /bin/bsh
$ cd /
$ chown -R bobby .
$ exit
-- back outside chroot --

-- inside chroot --
$ ssh bobby@ut30p30
bobby@ut30p30's password: 
$ ls /
QOpenSys  bin       dev       home      lib       sbin      tmp       usr
$ export PATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/bin
PATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/bin: is not an identifier
$ ksh
$ export PATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/bin
$ export LIBPATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/lib
$ node --version
v0.10.29

=======================
-- example 1st app ---
=======================
$ ssh bobby@ut30p30
bobby@ut30p30's password:
$ ksh
$ export PATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/bin
$ export LIBPATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/lib
$ cd
$ mkdir myapp
$ cd myapp
$ cat my1st.js
var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello World\n');
}).listen(8080, 'ut30p30');
console.log('Server running at http://ut30p30:8080/');
$ node my1st.js
Server running at http://ut30p30:8080/

http://myibmi:8080/
'Hello World'

============================================================
--- copy Ranger myibminode.js application (from Bluemix) ---
============================================================
Small 'hat chat' application that uses Yips IBM i via REST connection to XMLSERVICE.
> Web Application: http://myibminodejs.mybluemix.net/
> Git repository: https://hub.jazz.net/project/rangercairns/MyIBMiNodeJs/overview
  (bluemix application not always running, but code always available)

Download:
> download via button click: Branch: master |__|__|
> Download and unzip on your pc. Transfer all files up to IBM i
  /QOpenSys/bobby/home/bobby/MyIBMiNodeJs

$ ssh bobby@ut30p30
bobby@ut30p30's password:
$ ksh
$ export PATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/bin
$ export LIBPATH=/QOpenSys/QIBM/ProdData/Node/bin:/usr/lib
$ cd /home/bobby/MyIBMiNodeJs
$ ls 
License.txt            instructions.md        public
README.md              lib                    views
README.txt             manifest.yml           xmlserviceprovider.js
app.js                 package.json
-- not yet --
$ node app.js
module.js:340
    throw err;
Error: Cannot find module 'express'
$ npm install express
module.js:340
    throw err;
Error: Cannot find module 'npmlog'
-- fix --
$ cd /QOpenSys/QIBM/ProdData/Node/bin
$ ls ../lib/node_modules/npm/bin/npm-cli.js
../lib/node_modules/npm/bin/npm-cli.js
$ ln -sf ../lib/node_modules/npm/bin/npm-cli.js npm
$ ls -l npm
lrwxrwxrwx    1 bobby    0                76 Nov  9 07:48 npm -> ../lib/node_modules/npm/bin/npm-cli.js
$ cd
$ npm --version
1.4.14
-- npm install (error ok) --
$ npm install express
npm ERR! fetch failed https://registry.npmjs.org/buffer-crc32/-/buffer-crc32-0.2.1.tgz
npm ERR! fetch failed https://registry.npmjs.org/mkdirp/-/mkdirp-0.3.5.tgz
npm ERR! fetch failed https://registry.npmjs.org/fresh/-/fresh-0.2.0.tgz
npm ERR! fetch failed https://registry.npmjs.org/batch/-/batch-0.5.0.tgz
express@3.4.7 node_modules/express
-- not yet --
$ node app.js
App started on port 3000
events.js:72
        throw er; // Unhandled 'error' event
Error: listen EADDRINUSE
-- fix port in use --
$ export VCAP_APP_PORT=8081
$ export VCAP_APP_HOST=ut30p30
-- but not yet --
$ node app.js
App started on port 8081
module.js:340
    throw err;
          ^
Error: Cannot find module 'jade'
-- npm install (error ok) --
$ npm install jade
npm ERR! fetch failed https://registry.npmjs.org/character-parser/-/character-parser-1.2.0.tgz
jade@1.1.4 node_modules/jade
-- Yahoo, running --
$ node app.js
App started on port 8081

http://myibmi:8081/
"purple hats main page (ugly purple circles intentional, see stylesheet.css)"


