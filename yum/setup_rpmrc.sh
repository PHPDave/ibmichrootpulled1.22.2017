#!/bin/sh
#
# global
#

PATH=/opt/freeware/bin:/QOpenSys/usr/bin:/QOpenSys/usr/sbin
LIBPATH=""
export PATH
export LIBPATH
RPM_BUNDLE="rpm_bundle_v1"
OS400_BUNDLE="os400_bundle_v1"


function package_fixup_rpm {
  cdhere=$(pwd)
  echo "x            0 *** start IBM i compatible AIX toolbox changes *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i untar $OS400_BUNDLE *** "
  echo "x            0 tar -xf $OS400_BUNDLE.tar"
  tar -xf $OS400_BUNDLE.tar
  echo "x            0 *** end IBM i untar $OS400_BUNDLE *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** start IBM i rpm conf *** "
  echo "x            0 cd $OS400_BUNDLE/rpm-conf"
  cd $OS400_BUNDLE/rpm-conf
  unamem=$(uname -m)
  ureplace="s/REPLACEUNAME/$unamem/"
  echo "x            0 sed $ureplace rpmrc-os400 > rpmrc-this-machine"
  sed "$ureplace" rpmrc-os400 > rpmrc-this-machine
  echo "x            0 cp rpmrc-this-machine /opt/freeware/lib/rpm/rpmrc"
  cp rpmrc-this-machine /opt/freeware/lib/rpm/rpmrc
  echo "x            0 *** end IBM i rpm conf *** "
  echo "x            0 cd $cdhere"
  cd $cdhere

  echo "x            0 *** end IBM i compatible AIX toolbox changes *** "

}

#
# main
#
if [ -d /QOpenSys/usr/bin ]
then
  package_fixup_rpm
fi

