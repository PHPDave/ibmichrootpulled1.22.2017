# cmake tutorial
Using cmake on IBM i.

# prior sample
see cmake_bin (first)

# tutorial
```
$ export PATH=.:/opt/freeware/bin:/QOpenSys/usr/bin
$ export LIBPATH=/opt/freeware/lib:/QOpenSys/usr/lib
$ cd cmake_lib
$ mkdir build-dir
$ cd build-dir
$ cmake ..
```

